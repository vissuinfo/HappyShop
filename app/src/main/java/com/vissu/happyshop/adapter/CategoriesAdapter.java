package com.vissu.happyshop.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.vissu.happyshop.databinding.CategoryItemBinding;
import com.vissu.happyshop.listeners.OnCategoryClickListener;
import com.vissu.happyshop.model.Category;
import com.vissu.happyshop.viewholders.CategoryViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vissu on 7/24/17.
 */

public class CategoriesAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

    private List<Category> mCategories;
    private OnCategoryClickListener mCategoryClickListener;

    public CategoriesAdapter(OnCategoryClickListener listener) {
        mCategories = new ArrayList<>();
        this.mCategoryClickListener = listener;
    }

    public void updateData(List<Category> categories) {
        mCategories.clear();
        mCategories.addAll(categories);
        notifyDataSetChanged();
    }

    @Override
    public CategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        CategoryItemBinding binding = CategoryItemBinding.inflate(layoutInflater, parent, false);
        binding.setClickListener(mCategoryClickListener);
        return new CategoryViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(CategoryViewHolder holder, int position) {
        holder.bind(mCategories.get(position));
    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }
}
