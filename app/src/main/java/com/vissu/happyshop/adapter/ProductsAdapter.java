package com.vissu.happyshop.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.vissu.happyshop.databinding.ProductItemBinding;
import com.vissu.happyshop.listeners.OnProductClickListener;
import com.vissu.happyshop.model.Product;
import com.vissu.happyshop.viewholders.ProductViewHolder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by vissu on 7/24/17.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductViewHolder> {

    private List<Product> mProducts;
    private OnProductClickListener mProductClickListener;

    public ProductsAdapter(OnProductClickListener listener) {
        mProducts = new ArrayList<>();
        this.mProductClickListener = listener;
    }

    public void updateData(List<Product> products) {
        mProducts.clear();
        mProducts.addAll(products);
        notifyDataSetChanged();
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        ProductItemBinding binding = ProductItemBinding.inflate(layoutInflater, parent, false);
        binding.setClickListener(mProductClickListener);
        return new ProductViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        holder.bind(mProducts.get(position));
    }

    @Override
    public int getItemCount() {
        return mProducts.size();
    }
}
