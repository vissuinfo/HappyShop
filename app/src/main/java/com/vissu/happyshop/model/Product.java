package com.vissu.happyshop.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vissu on 7/24/17.
 */

public class Product {

    public Integer id;
    public String name;
    public String category;
    public Double price;
    @SerializedName("img_url")
    public String imageUrl;
    @SerializedName("under_sale")
    public Boolean underSale;
    public String description;// This will be used in product details screen

}
