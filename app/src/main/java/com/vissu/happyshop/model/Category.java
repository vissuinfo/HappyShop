package com.vissu.happyshop.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by vissu on 7/24/17.
 */

public class Category {

    public String name;
    @SerializedName("products_count")
    public Integer productsCount;
}
