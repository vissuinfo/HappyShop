package com.vissu.happyshop.response;

import com.vissu.happyshop.model.Category;

import java.util.List;

/**
 * Created by vissu on 7/24/17.
 */

public class CategoriesResponse {
    public List<Category> categories;
}
