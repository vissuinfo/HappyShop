package com.vissu.happyshop.response;

import com.vissu.happyshop.model.Category;
import com.vissu.happyshop.model.Product;

import java.util.List;

/**
 * Created by vissu on 7/24/17.
 */

public class ProductsResponse {
    public List<Product> products;
}
