package com.vissu.happyshop.fragments;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vissu.happyshop.R;
import com.vissu.happyshop.adapter.ProductsAdapter;
import com.vissu.happyshop.api.ApiClient;
import com.vissu.happyshop.databinding.FragmentListBinding;
import com.vissu.happyshop.listeners.OnCategoryClickListener;
import com.vissu.happyshop.listeners.OnProductClickListener;
import com.vissu.happyshop.response.ProductsResponse;
import com.vissu.happyshop.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductsFragment extends BaseFragment{

    private Call<ProductsResponse> mProductsApiCall;
    private FragmentListBinding binding;
    private ProductsAdapter mProductsAdapter;
    private OnProductClickListener mProductClcikListener;
    private String category;

    public ProductsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCategoryClickListener) {
            mProductClcikListener = (OnProductClickListener) context;
            category = getArguments().getString(Constants.PARAM_CATEOGRY);
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement OnProductClickListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false);
            binding.categoriesRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), 2));
            mProductsAdapter = new ProductsAdapter(mProductClcikListener);
            binding.categoriesRecyclerView.setAdapter(mProductsAdapter);
            fetchProducts();
        }
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(category);
        return binding.getRoot();
    }

    private void fetchProducts() {
        showProgress(String. format(getString(R.string.toast_fetch_products), category));
        mProductsApiCall = ApiClient.getService().fetchProducts(category);
        mProductsApiCall.enqueue(new Callback<ProductsResponse>() {
            @Override
            public void onResponse(Call<ProductsResponse> call, Response<ProductsResponse> response) {
                if(isAdded()) {
                    dismissProgress();
                    showProducts(response.body());
                }
            }

            @Override
            public void onFailure(Call<ProductsResponse> call, Throwable t) {
                if(isAdded()) {
                    dismissProgress();
                    toast(getString(R.string.toast_failure_fetch_products));
                    getFragmentManager().popBackStack();
                }
            }
        });
    }

    private void showProducts(ProductsResponse response) {
        if(response != null && response.products != null) {
            mProductsAdapter.updateData(response.products);
        }
    }
}
