package com.vissu.happyshop.fragments;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.widget.Toast;

/**
 * Created by vissu on 7/24/17.
 */

public class BaseFragment extends Fragment {

    private ProgressDialog progressDialog;

    protected void toast(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    protected void showProgress(String message) {
        if(progressDialog == null) {
            progressDialog = new ProgressDialog(getActivity());
            progressDialog.setCancelable(false);
        }
        progressDialog.setMessage(message);
        progressDialog.show();
    }

    protected void dismissProgress() {
        if(progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }
}
