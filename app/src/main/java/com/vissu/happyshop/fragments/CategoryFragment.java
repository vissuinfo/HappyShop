package com.vissu.happyshop.fragments;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vissu.happyshop.R;
import com.vissu.happyshop.adapter.CategoriesAdapter;
import com.vissu.happyshop.api.ApiClient;
import com.vissu.happyshop.databinding.FragmentListBinding;
import com.vissu.happyshop.listeners.OnCategoryClickListener;
import com.vissu.happyshop.response.CategoriesResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryFragment extends BaseFragment {

    private Call<CategoriesResponse> mCategoriesApiCall;
    private FragmentListBinding binding;
    private CategoriesAdapter mCategoriesAdapter;
    private OnCategoryClickListener mCategoryClickListener;

    public CategoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCategoryClickListener) {
            mCategoryClickListener = (OnCategoryClickListener) context;
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement OnCategoriesClickListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if(binding == null) {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_list, container, false);
            binding.categoriesRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mCategoriesAdapter = new CategoriesAdapter(mCategoryClickListener);
            binding.categoriesRecyclerView.setAdapter(mCategoriesAdapter);
            fetchCategories();
        }
        ((AppCompatActivity)getActivity()).getSupportActionBar().setTitle(R.string.categories);
        return binding.getRoot();
    }

    private void fetchCategories() {
        showProgress(getString(R.string.toast_fetch_categories));
        mCategoriesApiCall = ApiClient.getService().fetchCategories();
        mCategoriesApiCall.enqueue(new Callback<CategoriesResponse>() {
            @Override
            public void onResponse(Call<CategoriesResponse> call, Response<CategoriesResponse> response) {
                if(isAdded()) {
                    dismissProgress();
                    showCategories(response.body());
                }
            }

            @Override
            public void onFailure(Call<CategoriesResponse> call, Throwable t) {
                if(isAdded()) {
                    dismissProgress();
                    toast(getString(R.string.toast_failue_fetch_categories));
                    getActivity().finish();
                }
            }
        });
    }

    private void showCategories(CategoriesResponse response) {
        if (response != null && response.categories != null) {
            mCategoriesAdapter.updateData(response.categories);
        }
    }
}
