package com.vissu.happyshop.fragments;


import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.vissu.happyshop.R;
import com.vissu.happyshop.api.ApiClient;
import com.vissu.happyshop.databinding.FragmentDetailsBinding;
import com.vissu.happyshop.listeners.OnCartClickListener;
import com.vissu.happyshop.listeners.OnCartUpdateListener;
import com.vissu.happyshop.model.Product;
import com.vissu.happyshop.response.ProductResponse;
import com.vissu.happyshop.utils.CartUtils;
import com.vissu.happyshop.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsFragment extends BaseFragment implements OnCartClickListener {

    private Call<ProductResponse> mProductApiCall;
    private FragmentDetailsBinding mBinding;
    private OnCartUpdateListener mCartUpdateListener;
    private Integer mProductId;
    private String mProductName;

    public DetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mProductId = getArguments().getInt(Constants.PARAM_PRODUCT_ID);
        mProductName = getArguments().getString(Constants.PARAM_PRODUCT_NAME);
        if (getActivity() instanceof OnCartUpdateListener) {
            mCartUpdateListener = (OnCartUpdateListener) getActivity();
        } else {
            throw new ClassCastException(context.toString()
                    + " must implement OnCartUpdateListener");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (mBinding == null) {
            mBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_details, container, false);
            mBinding.setClickListener(this);
            mBinding.setIsAddedToCart(CartUtils.get(getActivity()).isAddedToCart(mProductId));
            fetchProducts();
        }
        ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(mProductName);
        return mBinding.getRoot();
    }

    private void fetchProducts() {
        showProgress(String.format(getString(R.string.toast_fetch_details), mProductName));
        mProductApiCall = ApiClient.getService().fetchProductDetails(mProductId);
        mProductApiCall.enqueue(new Callback<ProductResponse>() {
            @Override
            public void onResponse(Call<ProductResponse> call, Response<ProductResponse> response) {
                if(isAdded()) {
                    dismissProgress();
                    showProducts(response.body());
                }
            }

            @Override
            public void onFailure(Call<ProductResponse> call, Throwable t) {
                if(isAdded()) {
                    dismissProgress();
                    toast(getString(R.string.toast_failure_fetch_details));
                    getFragmentManager().popBackStack();
                }
            }
        });
    }

    private void showProducts(ProductResponse response) {
        if (response != null && response.product != null) {
            mBinding.setProduct(response.product);
        }
    }

    @Override
    public void onAddClick(Product product) {
        CartUtils.get(getActivity()).addToCart(product.id);
        mBinding.setIsAddedToCart(CartUtils.get(getActivity()).isAddedToCart(product.id));
        mCartUpdateListener.onCartUpdate();
    }

    @Override
    public void onRemoveClick(Product product) {
        CartUtils.get(getActivity()).removeFromCart(product.id);
        mBinding.setIsAddedToCart(CartUtils.get(getActivity()).isAddedToCart(product.id));
        mCartUpdateListener.onCartUpdate();
    }
}
