package com.vissu.happyshop.api;

import com.vissu.happyshop.response.CategoriesResponse;
import com.vissu.happyshop.response.ProductResponse;
import com.vissu.happyshop.response.ProductsResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by vissu on 7/24/17.
 */

public interface HappyShopInterface {

    @GET("categories.json")
    public Call<CategoriesResponse> fetchCategories();

    @GET("products.json")
    public Call<ProductsResponse> fetchProducts(@Query("category") String category);

    @GET("products/{product_id}.json")
    public Call<ProductResponse> fetchProductDetails(@Path("product_id") Integer productId);
}
