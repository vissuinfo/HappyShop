package com.vissu.happyshop.api;

import com.vissu.happyshop.utils.Constants;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by vissu on 7/24/17.
 */

public class ApiClient {
    public static Retrofit retrofit;

    public static HappyShopInterface getService() {
        if(retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(String.format("%s/%s/", Constants.BASE_URL, Constants.API_VERSION))
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getClient())
                    .build();
        }
        return retrofit.create(HappyShopInterface.class);
    }

    private static OkHttpClient getClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return new OkHttpClient.Builder().addInterceptor(interceptor).build();
    }
}
