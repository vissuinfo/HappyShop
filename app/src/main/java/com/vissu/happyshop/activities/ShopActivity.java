package com.vissu.happyshop.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.vissu.happyshop.R;
import com.vissu.happyshop.databinding.ActivityShopBinding;
import com.vissu.happyshop.fragments.CategoryFragment;
import com.vissu.happyshop.fragments.DetailsFragment;
import com.vissu.happyshop.fragments.ProductsFragment;
import com.vissu.happyshop.listeners.OnCartUpdateListener;
import com.vissu.happyshop.listeners.OnCategoryClickListener;
import com.vissu.happyshop.listeners.OnProductClickListener;
import com.vissu.happyshop.model.Category;
import com.vissu.happyshop.model.Product;
import com.vissu.happyshop.utils.CartUtils;
import com.vissu.happyshop.utils.Constants;

public class ShopActivity extends AppCompatActivity implements OnCategoryClickListener,
        OnProductClickListener, OnCartUpdateListener {

    private ActivityShopBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_shop);
        // Initialize the cart
        CartUtils.get(this).initCart();
        onCartUpdate();
        setSupportActionBar(mBinding.toolbar);

        // load categories fragment
        getFragmentManager().beginTransaction()
                .add(R.id.fragment_container, new CategoryFragment(),
                        CategoryFragment.class.getSimpleName()).commit();
    }

    @Override
    public void onCartUpdate() {
        mBinding.setCartcount(CartUtils.get(this).getCartCount());
    }

    @Override
    public void onCategoryClick(Category category) {
        // load products fragment
        Bundle bundle = new Bundle();
        bundle.putString(Constants.PARAM_CATEOGRY, category.name);
        ProductsFragment fragment = new ProductsFragment();
        fragment.setArguments(bundle);
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment,
                        ProductsFragment.class.getSimpleName())
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onProductClick(Product product) {
        // load products fragment
        Bundle bundle = new Bundle();
        bundle.putInt(Constants.PARAM_PRODUCT_ID, product.id);
        bundle.putString(Constants.PARAM_PRODUCT_NAME, product.name);
        DetailsFragment fragment = new DetailsFragment();
        fragment.setArguments(bundle);
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragment,
                        DetailsFragment.class.getSimpleName())
                .addToBackStack(null)
                .commit();
    }
}
