package com.vissu.happyshop.viewholders;

import android.support.v7.widget.RecyclerView;

import com.vissu.happyshop.databinding.CategoryItemBinding;
import com.vissu.happyshop.model.Category;

/**
 * Created by vissu on 7/24/17.
 */

public class CategoryViewHolder extends RecyclerView.ViewHolder {

    private CategoryItemBinding mBinding;

    public CategoryViewHolder(CategoryItemBinding binding) {
        super(binding.getRoot());
        this.mBinding = binding;
    }

    public void bind(Category category) {
        mBinding.setCategory(category);
    }

}
