package com.vissu.happyshop.viewholders;

import android.support.v7.widget.RecyclerView;

import com.vissu.happyshop.databinding.ProductItemBinding;
import com.vissu.happyshop.model.Product;

/**
 * Created by vissu on 7/24/17.Ø
 */

public class ProductViewHolder extends RecyclerView.ViewHolder {

    private ProductItemBinding mBinding;

    public ProductViewHolder(ProductItemBinding binding) {
        super(binding.getRoot());
        this.mBinding = binding;
    }

    public void bind(Product product) {
        mBinding.setProduct(product);
    }

}
