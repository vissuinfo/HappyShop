package com.vissu.happyshop.utils;

/**
 * Created by vissu on 7/24/17.
 */

public class Constants {

    public static final String BASE_URL = "http://sephora-mobile-takehome-apple.herokuapp.com/api";
    public static final String API_VERSION = "v1";

    public static final String PARAM_CATEOGRY = "category";
    public static final String PARAM_PRODUCT = "product";
    public static final String PARAM_PRODUCT_ID = "product_id";
    public static final String PARAM_PRODUCT_NAME = "product_name";
    public static final String PARAM_CART_COUNT = "cart_count";
    public static final String PARAM_CART_ITEMS = "cart_items";

}
