package com.vissu.happyshop.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by vissu on 7/24/17.
 */

public class CartUtils {

    SharedPreferences mPreferences;
    private static CartUtils sInstance;
    private int cartCount = 0;
    private Set<String> cartItems;

    private CartUtils() {
    }

    public static CartUtils get(Activity activity) {
        if(sInstance == null) {
            sInstance = new CartUtils();
            sInstance.mPreferences = activity.getPreferences(Context.MODE_PRIVATE);
            sInstance.initCart();
        } else {
            sInstance.mPreferences = activity.getPreferences(Context.MODE_PRIVATE);
        }
        return sInstance;
    }

    public void initCart() {
        cartCount = mPreferences.getInt(Constants.PARAM_CART_COUNT, 0);
        cartItems = mPreferences.getStringSet(Constants.PARAM_CART_ITEMS, new HashSet<String>());
    }

    public void addToCart(Integer productId) {
        if(productId != null) {
            SharedPreferences.Editor editor = mPreferences.edit();
            if(cartItems.add(productId.toString())) {
                editor.putStringSet(Constants.PARAM_CART_ITEMS, cartItems);
                editor.putInt(Constants.PARAM_CART_COUNT, ++cartCount);
            }
            editor.commit();
        }
    }

    public void removeFromCart(Integer productId) {
        if(productId != null) {
            SharedPreferences.Editor editor = mPreferences.edit();
            if(cartItems.remove(productId.toString())) {
                editor.putStringSet(Constants.PARAM_CART_ITEMS, cartItems);
                editor.putInt(Constants.PARAM_CART_COUNT, --cartCount);
            }
            editor.commit();
        }
    }

    public int getCartCount() {
        return new Integer(cartCount);
    }

    public boolean isAddedToCart(Integer productId) {
        return (productId != null && cartItems.contains(productId.toString()));
    }
}
