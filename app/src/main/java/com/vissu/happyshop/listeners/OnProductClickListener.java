package com.vissu.happyshop.listeners;

import com.vissu.happyshop.model.Category;
import com.vissu.happyshop.model.Product;

/**
 * Created by vissu on 7/24/17.
 */

public interface OnProductClickListener {
    void onProductClick(Product product);
}
