package com.vissu.happyshop.listeners;

/**
 * Created by vissu on 7/24/17.
 */

public interface OnCartUpdateListener {
    void onCartUpdate();
}
