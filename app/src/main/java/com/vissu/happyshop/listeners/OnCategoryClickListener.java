package com.vissu.happyshop.listeners;

import com.vissu.happyshop.model.Category;

/**
 * Created by vissu on 7/24/17.
 */

public interface OnCategoryClickListener {
    void onCategoryClick(Category category);
}
