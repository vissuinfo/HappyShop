package com.vissu.happyshop.listeners;

import com.vissu.happyshop.model.Product;

/**
 * Created by vissu on 7/24/17.
 */

public interface OnCartClickListener {
    void onAddClick(Product product);
    void onRemoveClick(Product product);
}
